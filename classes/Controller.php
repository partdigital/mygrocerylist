<?php
class Controller {
    
    protected $layoutTemplate = 'layout'; 
    
    protected $template; 
    
    protected $variables = array(); 
    
    public function __construct() {
        
    }
    
    /**
    * Pre-render function. 
    */ 
    protected function preRender() {
        
    }
    
    /**
    * Render function. 
    */ 
    protected function render() {
        
        $this->preRender(); 
        
        // Load the content. 
        $content = new Template($this->template, $this->variables); 
        
        // Generate the menu. 
        $menu = array(
            'index.php' => 'Home',
            'item.php' => 'Add Grocery Item',
            'list.php' => 'List Grocery Items',
            'user.php' => 'Add User',
            'users.php' => 'List Users',
            'logout.php' => 'Log Out'
        );
        
        $myMenu = new Menu($menu);  
    
        // Final Render 
        $layoutVariables = array(
            'menu' => $myMenu->render(),
            'content' => $content->render(),
        );
            
       $page = new Template($this->layoutTemplate, $layoutVariables); 
        
       return $page->render(); 
    }
    
    /**
    * Run the controller. 
    */ 
    public function run() {
        return $this->render(); 
    }
}