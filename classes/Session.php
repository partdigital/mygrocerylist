<?php

class Session {
    
    /**
    * Start the session.
    */ 
    public function start() {
        session_start(); 
    }
    
    /**
    * Is user authorized?
    */ 
    public function isAuthorized() {
        // If not logged in 
        if (empty($_SESSION['auth'])) {
            // redirect to login.php
            header('location:login.php'); 
        }  
    }
    
    /**
    * Login the user.
    */ 
    public function login($user_id) {
        $_SESSION['auth'] = TRUE; 
        $_SESSION['user_id'] = $user_id; 
    }
    
    /**
    * Log out and destroy session.
    */ 
    public function logout() {
        unset($_SESSION['auth']); 
        unset($_SESSION['user_id']); 
        session_destroy(); 
    }
}