<?php

class User extends Model {
    
    public function insert($variables) {
        $variables['password'] = $this->encryptPassword($variables['password']); 
        
        return $this->db->query("INSERT INTO `users` (`username`, `password`) VALUES (':username', ':password')", $variables); 
    }
    
    private function encryptPassword($string) {
        return md5($string); 
    }
    
    public function update($variables, $id) {
        
        $variables['password'] = $this->encryptPassword($variables['password']); 
        
        return $this->db->query("UPDATE `users` SET 
            `username` = ':username',
            `password` = ':password'
            WHERE `id` = '" . $id . "'", $variables);
    }
    
    public function delete($id) {
        $this->db->query("DELETE FROM `users` WHERE `id` = '" . $id ."'"); 
    }
    
    public function fetchAll() {
        return $this->db->select("SELECT * FROM `users`"); 
    }
    
    public function fetch($item_id) {
        $items = $this->db->select("SELECT * FROM `users` WHERE `id` = '" . $item_id ."'"); 
        return $items[0]; 
    }
    
    public function authenticate($username, $password) {
        
       $password = $this->encryptPassword($password); 
        
       $user = $this->db->select('SELECT * FROM users WHERE `username` = "'.$username.'" AND `password` = "'.$password.'"'); 	
       if (!empty($user)) {
           return $user[0]; 
       }

       return FALSE; 
    }
}