<?php include 'bootstrap.php';

class ListController extends Controller {
    
    
    // Template;
    protected $template = 'list'; 
    
    protected $variables = array(
        'list' => '',
    ); 
    
    /**
    * Prerender function.
    */ 
    public function preRender() {
        // Fetching groceries
        // Query statement 
        $item = new Item(); 
        $data = $item->fetchUserItems($_SESSION['user_id']); 

        if(!empty($data)) { 
            // Start Table tag 
            $table =  '<table>';  

            // Loop through each row in the database 
            // Store each row as an associative array 
            foreach ($data as $row) {
                // Opening row tag 
                $table .= '<tr>'; 

                // Output the name
                $table .= '<td>' . $row['name'] . '</td>';

                // Output quantity 
                $table .= '<td>' . $row['qty'] . '</td>'; 

                // Output Edit/Delete links with query strings. 
                $table .= '<td>';
                $table .= '<a href = "item.php?id=' . $row['id'] . '">Edit</a> | '; 
                $table .= '<a href = "item-delete.php?id=' . $row['id'] . '">Delete</a>'; 
                $table .= '</td>'; 
                // Closing row tag 
                $table .= '</tr>'; 

            }
            // Closing table tag 
            $table .= '</table>'; 
            
            $this->variables['list'] = $table; 
        }
    }
}
$session->isAuthorized();
$controller = new ListController(); 
print $controller->run(); 