<?php 
include 'bootstrap.php'; 

class LogoutController extends Controller {
    
    protected $template = 'logout'; 
    
    protected $layoutTemplate = 'layout-no-menu'; 
    
    public function preRender() {
        global $session; 
        $session->logout(); 
    }
}

$session->isAuthorized(); 
$controller = new LogoutController(); 
print $controller->run(); 
